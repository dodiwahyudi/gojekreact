import React, { Component } from 'react'
import Image1 from "../Image/a89e28c175e85c47fea32bc10b4147c7.jpg"

export default class TentangDameko extends Component {
  render() {
    return (
      <div>
        <div className="rowTentangLayanan row justify-content-around align-items-center ">
          <div className="dampekText col-sm-5 d-flex flex-column justify-content-center align-items-start">
            <h2 className="font-weight-bold">Cerdikiawan</h2>
            <h2 className="font-weight-bold">#PastiAdaJalan</h2>
            <p>Semangat Cerdikiawan tercermin dalam Gojek, yang awalnya terbentuk untuk mengakali kemacetan dan permasalahan transportasi. Kini Gojek menyediakan beragam layanan untuk solusi sehari-hari. </p>
            <p>Harus ngantor lebih awal? Bisa pakai GoCar untuk nambah jam tidur. Hari pertama di kantor baru? Bisa pakai GoFood buat nambah kenalan. Apa pun permasalahannya, pasti ada jalan dengan Gojek.</p>
         </div>
          <div className="dampekImg col-sm-4">
          <img className = "imgTtgDameko" src={Image1} alt=" "></img>
          </div>
        </div>
      </div>
    )
  }
}

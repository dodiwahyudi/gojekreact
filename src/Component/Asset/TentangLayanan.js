import React, { Component } from 'react'
import Image1 from "../Image/c5a2d28d51a866f4fb171a2e66df817a.jpg"

export default class TentangLayanan extends Component {
  render() {
    return (
      <div>
        <div className="rowTentangLayanan row justify-content-around align-items-center">
          <div className="col-sm-5">
          <img className = "imgTtgLayanan" src={Image1} alt=" "></img>
          </div>
          <div className="row3Desc col-sm-4 d-flex flex-column justify-content-center align-items-start">
              <h2 className="font-weight-bold">Layanan</h2>
              <p>Lewat aplikasi Gojek, kamu bisa mengakses lebih dari 20 layanan mulai dari transportasi, pesan antar makanan, belanja, kirim-kirim barang, pembayaran, pijat, sampai bersih-bersih rumah dan kendaraan. Karena Gojek adalah aplikasi dengan ragam solusi untuk setiap situasi.</p>
          </div>
        </div>
      </div>
    )
  }
}

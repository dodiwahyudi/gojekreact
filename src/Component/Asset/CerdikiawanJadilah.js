import React, { Component } from 'react'
import Image1 from "../Image/c5a2d28d51a866f4fb171a2e66df817a.jpg"

export default class CerdikiawanJadilah extends Component {
  render() {
    return (
      <div>
        <div className="rowCerdikiawanJadilah row justify-content-around align-items-center">
          <div className="col-sm-5">
          <img className = "imgTtgLayanan" src={Image1} alt=" "></img>
          </div>
          <div className="row3Desc col-sm-4 d-flex flex-column justify-content-center align-items-start">
              <h2 className="font-weight-bold">Jadilah Cerdikiawan</h2>
              <p>Kamu juga bisa jadi Cerdikiawan bersama Gojek. Persiapkan inovasi cerdik versi kamu untuk di-upload dan menangkan berbagai hadiah menarik! Pantau terus halaman ini untuk jadi yang pertama ikutan kompetisi Inovasi Cerdikiawan.</p>
          </div>
        </div>
      </div>
    )
  }
}

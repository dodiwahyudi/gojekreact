import React, { Component } from 'react'
import "../Style/style.css"
import Image1 from "../Image/speed.svg"
import Image2 from "../Image/innovation.svg"
import Image3 from "../Image/social-impact.svg"

export default class TentangTigaPilar extends Component {
  render() {
    return (
      <div className="containerPilar">
        <div className="row ">
          <div className="col-md-12 text-center ">
            <h1>Tiga Pilar Gojek</h1>
          </div>
        </div>
      <div className="row d-flex justify-content-around ">
        <div className="col-md-3 ml-5 ">
          <div className="row d-flex justify-content-center ">
            <img src={Image1} alt=""></img>
          </div>
          <div className="row d-flex justify-content-center ">
            <h3>Kecepatan</h3>
          </div>
          <div className="row d-flex text-center ">
            <p>Kami melayani dengan cepat, serta akan terus berkembang dan belajar dari pengalaman.</p>
          </div>
        </div>

        <div className="col-md-3 ">
          <div className="row d-flex justify-content-center ">
            <img src={Image2} alt=""></img>
          </div>
          <div className="row d-flex justify-content-center ">
            <h3>Inovasi</h3>
          </div>
          <div className="row d-flex text-center ">
            <p>Kami akan terus berkarya untuk memperbaiki layanan kami untuk senantiasa memberikan kemudahan bagi seluruh pengguna.</p>
          </div>
        </div>

        <div className="col-md-3 mr-5 ">
          <div className="imagePilar row d-flex justify-content-center ">
            <img src={Image3} alt=""></img>
          </div>
          <div className="row d-flex justify-content-center ">
            <h3>Kecepatan</h3>
          </div>
          <div className="row d-flex text-center ">
            <p>Kami melayani dengan cepat, serta akan terus berkembang dan belajar dari pengalaman.</p>
          </div>
        </div>
      </div>
    </div>
    )
  }
}

import React, { Component } from 'react'
import "../Style/style.css"
import Fb from "../Image/fb.png"
import Ig from "../Image/ig.png"
import Twit from "../Image/twit.png"
import Linkedin from "../Image/linkedin.png"
import Youtube from "../Image/youtube.png"

export default class TentangOfficial extends Component {
  render() {
    return (
    <div className="containerOfficial">
      <div className="judul col-md-12 text-center ">
        <h1>Official Channels</h1>
      </div>
      <div className="row">
        <div className="col-md-1">
        </div>
        <div className="col-md-10">

          <div class="grid-container">
            <div class="Gojekindo">
              <h3>Gojek Indonesia</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@gojekindonesia</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" "></img>
                  <p>@gojekindonesia</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Twit} alt=" "></img>
                  <p>@gojekindonesia</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Linkedin} alt=" "></img>
                  <p>@gojekindonesia</p>
                </li>
              </ul>
            </div>
            <div class="Gopoints">
            <h3>GoPoints</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@gopointsindonesia</p>
                </li>
              </ul>
            </div>
            <div className="Gobox">
              <h3>GoBox</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@goboxindonesia</p>
                </li>
              </ul>
            </div>
            <div className="Gosend">
            <h3>GoSend</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@gosendindonesia</p>
                </li>
              </ul>
            </div>
            <div className="Gotix">
              <h3>GoTix</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@gotixindonesia</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Twit} alt=" "></img>
                  <p>@gotixindonesia</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" "></img>
                  <p>GoTix Indonesia</p>
                </li>
              </ul>
            </div>
            <div className="Golife">
              <h3>Golife</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@golifeindonesia</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Twit} alt=" "></img>
                  <p>@golifeindonesia</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" "></img>
                  <p>GoLife Indonesia</p>
                </li>
              </ul>
            </div>
            <div className="Gopromo">
              <h3>Gojek Lorem</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@gojeklorem</p>
                </li>
              </ul>
            </div>
            <div className="Gopay">
              <h3>Gojek Lorem</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@gojeklorem</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Twit} alt=" " ></img>
                  <p>@gojeklorem</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" "></img>
                  <p>GoLorem Ipsum</p>
                </li>
              </ul>
            
            </div>
            <div className="Gofood">
              <h3>Gojek Lorem</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" " ></img>
                  <p>@gojeklorem</p>
                </li>
              </ul>

            </div>
            <div className="Goviet">
              <h3>GO-VIET (Vietnam)</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" " ></img>
                  <p>Facebook (driver page)</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" "></img>
                  <p>GO-VIET fanpage</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Youtube} alt=" "></img>
                  <p>GO-VIET YouTube page</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Youtube} alt=" "></img>
                  <p>GO-FOOD YouTube page</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" "></img>
                  <p>GO-VIET Instagram</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" "></img>
                  <p>GO-FOOD Instagram</p>
                </li>
              </ul>
            </div>
            <div className="Gosinga">
              <h3>Gojek Singapore</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" " ></img>
                  <p>Facebook</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Ig} alt=" "></img>
                  <p>Instagram</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Twit} alt=" "></img>
                  <p>Twitter</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Linkedin} alt=" "></img>
                  <p>Web</p>
                </li>
              </ul>
            </div>
            <div className="Getthai">
              <h3>GET! (Thailand)</h3>
              <ul>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" " ></img>
                  <p>Facebook (user page)</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Fb} alt=" "></img>
                  <p>Facebook (driver page)</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Linkedin} alt=" "></img>
                  <p>Linkedin</p>
                </li>
                <li className="d-flex">
                  <img className="imgLogo" src={Twit} alt=" "></img>
                  <p>Twitter</p>
                </li>
              </ul>
            </div>
            </div>
          </div>
        <div className="col-md-1">
        </div>
      </div>

    </div>
    )
  }
}

import React, { Component } from 'react'
import Image1 from "../Image/bf661495fda5c3d61c291a626de6734e.jpg"

export default class CerdikiawanLayanan extends Component {
  render() {
    return (
      <div>
        <div className="rowTentangLayanan row justify-content-around align-items-center">
          <div className="col-sm-5">
          <img className = "imgTtgLayanan" src={Image1} alt=" "></img>
          </div>
          <div className="row3Desc col-sm-4 d-flex flex-column justify-content-center align-items-start">
              <h2 className="font-weight-bold">Kawanan Cerdik Anak Bangsa</h2>
              <p>Cerdikiawan adalah anak bangsa yang penuh akal. Mereka berkarya dengan modal kecerdikan, untuk menuntaskan semua permasalahan. </p>
              <p>Saking cerdiknya, gak ada keterbatasan yang bisa menghentikan. Cerdikiawan selalu punya gagasan solutif untuk mengakali keadaan.</p>
          </div>
        </div>
      </div>
    )
  }
}

import React, { Component } from 'react'
import Gambar1 from "../Image/50296bfc49d70e5c0bf1fa39bd9aa077.jpg"
import Gambar2 from "../Image/3ebb17ae51f83bf6491940ef149a0be8.jpg"

export default class TentangSemangat extends Component {
  render() {
    return (
      <div>
        <div class="container-fluid">
          <div class=" rowtk1 row align-items-center border">
            <div class="col-md-1 ">
            </div>
            <div class=" col-md-6 ">
              <h1>Semangat baru Gojek</h1>
              <p>Kenalin logo baru Gojek: Solv 
                Solv berangkat dari mimpi Gojek yang ingin membantu memudahkan kehidupan orang banyak melalui teknologi.</p>

              <p>Berawal dari layanan transportasi, sekarang aplikasi Gojek memiliki lebih dari 20 layanan yang menjadi solusi buat tantangan sehari-hari. 
                Berkat itu juga, Gojek menjadi salah satu platform teknologi terbesar yang melayani jutaan pengguna di Asia Tenggara dengan mengembangkan 
                tiga Super-app: untuk customer, untuk mitra driver, dan juga mitra merchant. </p>

              <p>Solv menjadi simbol yang mengingatkan kita semua kalau Gojek punya berbagai solusi, untuk setiap situasi. Memberikan kamu power untuk melewati keribetan sehari-hari.
                Pengingat bahwa di balik setiap tantangan, pasti ada solusi untuk melewatinya. Karena dengan Gojek, #PastiAdaJalan</p>
            </div>
            <div class="semangatImg col-md-4 align-items-center ">
              <img src={Gambar1} alt=" "></img>
            </div>
            <div class="col-md-1 ">
            </div>
          </div>

        <div class="row rowtk1 align-items-center ">
          <div class="col-md-1">
          </div>
          <div class="perjalananImg col-md-4 align-items-center">
            <img src={Gambar2} alt=" "></img>
          </div>
          <div class="col-md-6 align-items-center">
            <h1>Perjalanan Gojek</h1>

            <p>Gojek memulai perjalanannya pada tahun 2010 dengan layanan 
              pertama kami yaitu pemesanan ojek melalui call-center. </p>

            <p>Pada tahun 2015, Gojek berkembang pesat setelah meluncurkan sebuah aplikasi dengan tiga layanan,
              yaitu: GoRide, GoSend, dan GoMart.</p>

            <p>Sejak saat itu, laju Gojek semakin cepat dan terus beranjak hingga menjadi grup teknologi terkemuka 
              yang melayani jutaan pengguna di Asia Tenggara.</p>
          </div>
          <div class="col-md-1">
          </div>
          </div>
          <div class="row">
            <div class="col-md-12">
          </div>
          </div>
        </div>
      </div>
    )
  }
}

import React, { Component } from 'react'
import Image1 from "../Image/b7493cac8b4f4e3c0d2efb59e07dd214.jpg"

export default class CerdikiawanInov extends Component {
  render() {
    return (
      <div>
        <div className="rowTentangLayanan row justify-content-around align-items-center ">
          <div className="dampekText col-sm-5 d-flex flex-column justify-content-center align-items-start">
            <h2 className="font-weight-bold">Tunjukkan inovasi </h2>
            <h2 className="font-weight-bold">cerdikmu!</h2>
            <p>Foto inovasi cerdikmu dengan Instagram filter Cerdikiawan di Instagram @gojekindonesia dan upload ke sosial mediamu. Jangan lupa tag kita ya!</p>  
         </div>
          <div className="dampekImg col-sm-4">
          <img className = "imgTtgDameko" src={Image1} alt=" "></img>
          </div>
        </div>
      </div>
    )
  }
}

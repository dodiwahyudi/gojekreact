import React, { Component } from 'react'
import Image1 from "../Image/bfcdcf7c3f912e4cc6f0be515e280c30.jpg"

export default class TentangDameko extends Component {
  render() {
    return (
      <div>
        <div className="rowTentangLayanan row justify-content-around align-items-center ">
          <div className="dampekText col-sm-5 d-flex flex-column justify-content-center align-items-start">
              <h2 className="font-weight-bold">Dampak Ekonomi Sosial</h2>
              <p>Riset oleh Lembaga Demografi Fakultas Ekonomi & Bisnis, Universitas Indonesia, tahun 2018, melibatkan 6.732 responden di 9 kota di Indonesia.</p>
              <h5>Memberikan Dampak Ekonomi Untuk Indonesia </h5>
              <p>Gojek menyumbang sekitar Rp44,2 triliun (US $ 3 miliar) bagi perekonomian Indonesia pada akhir 2018 *. </p>
              <h5>Membantu Anggota Di Ekosistem Kami Mitra driver</h5>
              <p>Sejak bergabung dengan Gojek, kualitas hidup mitra driver meningkat - 100%. Mitra driver kami percaya bahwa dengan skema insentif dan kebijakan yang diterapkan Gojek, mereka dapat menyejahterakan keluarga mereka. Sebagian besar dari mereka mengklaim bahwa mereka sekarang dapat menyekolahkan anaknya.</p>
          </div>
          <div className="dampekImg col-sm-4">
          <img className = "imgTtgDameko" src={Image1} alt=" "></img>
          </div>
        </div>
      </div>
    )
  }
}

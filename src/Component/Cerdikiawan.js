import React, { Component } from 'react'
import Header from "./Asset/Header" 
import Jumbotron from "./Asset/Jumbotron"
import CerdikiawanLayanan from "./Asset/CerdikiawanLayanan"
import CerdikiawanPaj from "./Asset/CerdikiawanPaj"
import CerdikiawanJadilah from "./Asset/CerdikiawanJadilah"
import CerdikiawanAwards from "./Asset/CerdikiawanAwards"
import CerdikiawanInov from "./Asset/CerdikiawanInov"
import CerdikiawanMandi from "./Asset/CerdikiawanMandi"

import Footer from "./Asset/Footer" 

export default class Cerdikiawan extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Jumbotron/>
        <CerdikiawanLayanan/>
        <CerdikiawanPaj/>
        <CerdikiawanJadilah/>
        <CerdikiawanAwards/>
        <CerdikiawanInov/>
        <CerdikiawanMandi/>
        <Footer/>
      </div>
    )
  }
}

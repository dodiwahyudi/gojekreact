import React, { Component } from 'react'
import Header from "./Asset/Header"
import Footer from "./Asset/Footer"
import TentangSemangat from "./Asset/TentangSemangat"
import TentangTiga from "./Asset/TentangTiga"
import TentangLayanan from "./Asset/TentangLayanan"
import TentangDameko from "./Asset/TentangDameko"
import TentangPenghargaan from "./Asset/TentangPenghargaan"
import TentangOfficial  from "./Asset/TentangOfficial.js"

export default class Tentang extends Component {
  render() {
    return (
      <div>
        <Header/>
        <TentangSemangat/>
        <TentangTiga/>
        <TentangLayanan/>
        <TentangDameko/>
        <TentangPenghargaan/>
        <TentangOfficial/>
        <Footer/>
      </div>
    )
  }
}
